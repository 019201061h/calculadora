<?php
    class Calculadora{
        //atributos
        public $nro1;
        public $nro2;
        //Metodos u operaciones
        public function Sumar()
        {
            return $this->nro1 + $this->nro2;
        }
        public function Restar()
        {
            return $this->nro1 - $this->nro2;
        }
        //Multiplicar y dividir
        public function Multiplicar()
        {
            return $this->nro1 * $this->nro2;
        }
        public function Dividir()
        {
            if($this->nro2==0)
                return "Error división entre 0";
            else
                return $this->nro1 / $this->nro2;
            
        }
        private function Fact($nro)
        {
            if($nro == 0)
                return 1;
            else
                return $nro * $this->Fact($nro - 1);
        }
        public function Factorial()
        {
            return $this->Fact($this->nro1);
        }
        //potencia, seno y tangente
        private function Pot($nro1,$nro2)
        {
            if($nro2 == 0 )
                return 1;
            else
                return $nro1 * $this->Pot($nro1,$nro2-1);
        }
        public function Potencia()
        {
            return $this->Pot($this->nro1,$this->nro2);
        }

        private function Sen($nro1)
        {
            return(sin(deg2rad($nro1)));
        }
        public function Seno()
        {
            return $this->Sen($this->nro1);
        }
        public function Tang($nro1)
        {
            return(tan(deg2rad($nro1)));
        }
        public function Tan()
        {
            return $this->Tang($this->nro1);
        }
        public function Porc($nro1)
        {
            return($nro1/100);
        }
        public function Porcentaje()
        {
            return $this->Porc($this->nro1);
        }
        public function RaizCu($nro1)
        {
            return(sqrt($nro1));
        }
        public function RaizCuadrada()
        {
            return $this->RaizCu($this->nro1);
        }
        public function RaizN($nro1,$nro2)
        {
            if($this->nro1==0)
                return "Error de raiz 0";
            else
            return(pow($nro2,(1/$nro1)));
        }
        public function RaizNsima()
        {
            return $this->RaizN($this->nro1,$this->nro2);
        }
        public function Inv($nro1)
        {
            if($this->nro1==0)
                return "Error de inversa de 0";
            else
            return(pow($nro1,(-1)));
        }
        public function Inversa()
        {
            return $this->Inv($this->nro1);
        }
    }
?>