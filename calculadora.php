<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Calculadora</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <center>
                <br>
                <br>
                <h3>
                    Calculadora
                </h3>
                <br>
                <form method="POST" action="#" >

                    <p><h3>Nro1: <input type="text" name="txtNro1" size="40" maxlength="30"/></h3> </p>
                    <p><h3>Nro2: <input type="text" name="txtNro2" size="40" maxlength="30"/></h3> </p>
                </center>
                <br>
                <br>
                <center>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-success" value="+" name="btnSumar">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-warning" value="-" name="btnRestar">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-info" value="!" name="btnFactorial">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-danger btn-lg" value="^" name="btnPotencia">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-success" value="%" name="btnPorcentaje">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-warning" value="^(1/2)" name="btnRaizCuadrada">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-danger btn-lg" value="x" name="btnMultiplicar">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-info" value="/" name="btnDividir">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-warning" value="Sen" name="btnSeno">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-success" value="Tan" name="btnTangente">
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-danger btn-lg" value="^(1/n)" name="btnRaizN">
                            </div>
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-lg btn-info" value="Inv" name="btnInversa">
                            </div>
                        </div>
                    </div>
                </div>
                </form>
                <br>
                <br>
                <h3>
                <?php
                    if($_POST){
                        include("calculadora1.php");
                        $nro1 = $_POST['txtNro1'];
                        $nro2 = $_POST['txtNro2'];
                        if(isset($_POST['btnSumar']))
                        {
                            //Instanciar un onjeto a traves de la clase
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $calculo->nro2 = $nro2;
                            $suma = $calculo->Sumar();
                            echo "La suma de los numeros ",$nro1," y ",$nro2," es: ", $suma;
                        }
                        else if(isset($_POST['btnRestar']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $calculo->nro2 = $nro2;
                            $resta = $calculo->Restar();
                            echo "La resta de los numeros ",$nro1," y ",$nro2," es: ", $resta;
                        }
                        else if(isset($_POST['btnMultiplicar']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $calculo->nro2 = $nro2;
                            $multi = $calculo->Multiplicar();
                            echo "La multiplicación de los numeros ",$nro1," y ",$nro2," es: ", $multi;
                        }
                        else if(isset($_POST['btnDividir']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $calculo->nro2 = $nro2;
                            $div = $calculo->Dividir();
                            echo "La división de los numeros ",$nro1," entre ",$nro2," es: ", $div;
                        }
                        else if(isset($_POST['btnFactorial']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $fact = $calculo->Factorial();
                            echo "El factorial del número ",$nro1," es: ", $fact;
                        }
                        else if(isset($_POST['btnPotencia']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $calculo->nro2 = $nro2;
                            $pot = $calculo->Potencia();
                            echo "La potencia del número ",$nro1," elevado a ",$nro2," es: ", $pot;
                        }
                        else if(isset($_POST['btnSeno']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $sen = $calculo->Seno();
                            echo "El seno de ",$nro1,"° es: ", $sen;
                        }
                        else if(isset($_POST['btnTangente']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $tan = $calculo->Tan();
                            echo "La tangente de ",$nro1,"° es: ", $tan;
                        }
                        else if(isset($_POST['btnPorcentaje']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $porc = $calculo->Porcentaje();
                            echo $nro1,"% es: ", $porc;
                        }
                        else if(isset($_POST['btnRaizCuadrada']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $raizQ = $calculo->RaizCuadrada();
                            echo "La raíz cuadrada del número ",$nro1," es: ", $raizQ;
                        }
                        else if(isset($_POST['btnRaizN']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $calculo->nro2 = $nro2;
                            $raizN = $calculo->RaizNsima();
                            echo "La raiz ",$nro1," del número ",$nro2," es: ", $raizN;
                        }
                        else if(isset($_POST['btnInversa']))
                        {
                            $calculo = new Calculadora;
                            $calculo->nro1 = $nro1;
                            $inv = $calculo->Inversa();
                            echo "La inversa del número ",$nro1," es: ", $inv;
                        }
                        }
    
    ?>
    </h3>
                </center>
                
            </div>
        </div>
        
    </div>
    
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    
  

</body>

</html>

